### Dependences
For build TCP server need libpoco-dev.
On Ubuntu:
```
$ apt-get install libpoco-dev
```
### Build
For build TCP server run command:
```
$ ./build.sh
```
### Run
To start TCP server run command:
```
$ ./bin/SimpleServer
```
