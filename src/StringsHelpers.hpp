#pragma once

#include <algorithm>

inline static bool StringIsEmpty(const char* string)
{
    return string[0] == '\0';
}

inline char* ReverseString(char* string)
{
    if (!string || StringIsEmpty(string)) return string;

    int len = strlen(string);

    for (int idx = 0; idx < len / 2; idx++)
    {
        std::swap(string[idx], string[len - idx - 1]);
    }

    return string;
}
