#pragma once

#include "Poco/Net/TCPServer.h"
#include "Poco/Net/TCPServerConnection.h"
#include "Poco/Net/TCPServerConnectionFactory.h"
#include "Poco/Util/ServerApplication.h"

class SimpleServerConnection: public Poco::Net::TCPServerConnection
{
private:

    static constexpr auto kBufferSize = 256;

    char _buffer[kBufferSize];

public:

    SimpleServerConnection(const Poco::Net::StreamSocket& socket);

    void run();
};


class SimpleServerConnectionFactory: public Poco::Net::TCPServerConnectionFactory
{
public:

    Poco::Net::TCPServerConnection* createConnection(const Poco::Net::StreamSocket& socket)
    {
        return new SimpleServerConnection(socket);
    }
};


class SimpleServer: public Poco::Util::ServerApplication
{
private:

    void initialize(Application& self) { ServerApplication::initialize(self); }

    void uninitialize() { ServerApplication::uninitialize(); }

    int main(const std::vector<std::string>& args);
};
