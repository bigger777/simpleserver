#include "SimpleServer.hpp"

int main(int argc, char** argv)
{
    SimpleServer server;

    return server.run(argc, argv);
}
