#include "SimpleServer.hpp"
#include "StringsHelpers.hpp"

SimpleServerConnection::SimpleServerConnection(const Poco::Net::StreamSocket& socket) :
    Poco::Net::TCPServerConnection(socket) {}

void SimpleServerConnection::run()
{
    Poco::Util::Application& app = Poco::Util::Application::instance();

    app.logger().information("Connection from " + this->socket().peerAddress().toString());

    static constexpr char kWelcomeMessage[] = "Welcome to SimpleServer. Enter your string:\n";

    socket().sendBytes(kWelcomeMessage, sizeof(kWelcomeMessage));

    while (true)
    {
        std::memset(_buffer, 0, sizeof(_buffer));

        try
        {
            const auto receivedBytes = socket().receiveBytes(_buffer, sizeof(_buffer) - 1);

            socket().sendBytes(ReverseString(_buffer), receivedBytes);
        }
        catch (Poco::Exception& exc)
        {
            app.logger().log(exc);
        }
    }
}

int SimpleServer::main(const std::vector<std::string>& args)
{
    static constexpr auto kPort = 28888;

    Poco::Net::ServerSocket serverSocket(kPort);

    Poco::Net::TCPServer server(new SimpleServerConnectionFactory(), serverSocket);

    server.start();

    waitForTerminationRequest();

    server.stop();

    return Application::EXIT_OK;
}
